package net.example.farest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarestApplication.class, args);
	}

}
